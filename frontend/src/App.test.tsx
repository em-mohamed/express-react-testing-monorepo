// src/App.test.tsx
import { render, screen } from '@testing-library/react';
import App from './App';

it('renders without crashing', () => {
   // Rendu du composant App dans le DOM virtuel
  render(<App />);

  // Récupération de l'élément titre par son texte
  const titleElement = screen.getByText(/Vite \+ React/i);

  // Assertions pour vérifier la présence du titre et sa classe CSS
  expect(titleElement).toBeInTheDocument();
  expect(titleElement).toHaveClass('App-title');
});
